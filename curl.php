<?php
/**
 * This is a simple library for handling HTTP requests using curl.
 * This project is for demonstration purposes, and should be used as a learning
 * resources only. This library demonstrates how to create reusable functionality
 * and using the DRY principle in your code.
 *
 * And Remember DocBlocks are handy with an IDE
 *
 * @note   This file also follows a blended coding styling of PSR-1 and rules of PSR-12
 * @author jlisher <Look me up>
 */

/**
 * Lets first create some request method constants that we can use to ensure
 * that typos don't get in the way, and take advantage of completion functionality.
 */
const HTTP_GET = 'GET';
const HTTP_POST = 'POST';

/**
 * A method used to simple do the curl request and return a formatted response.
 *
 * @param string $url The URL endpoint to submit the request to.
 * @param string $method The HTTP method to be used (default: GET)
 * @param array $data The data used to be submitted with the request (default: [])
 * @param array $headers The request headers to be send with the request (default: [])
 *
 * @return array
 * @throws JsonException
 */
function doRequest(string $url, string $method = HTTP_GET, array $data = [], array $headers = []): array
{
    /**
     * Lets initialise the curl handle.
     */
    $handle = curl_init();

    /**
     * Now we set some of the common options that must exist in all requests.
     */
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);

    if (in_array('Content-Type: application/json', $headers, true)) {

    }

    /**
     * Now we simply going to set the curl options that are based on the method
     * to be used.
     */
    switch ($method) {
        case HTTP_GET:
            $query = http_build_query($data);
            curl_setopt($handle, CURLOPT_URL, $url . '?' . $query);
            curl_setopt($handle, CURLOPT_HTTPGET, true);
            break;

        case HTTP_POST:
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
            curl_setopt($handle, CURLOPT_POST, true);
            break;
    }

    /**
     * Lets finally perform the request
     */
    $response = curl_exec($handle);

    /**
     * Lets check if we got a curl error.
     */
    if (curl_errno($handle)) {
        $error_message = curl_errno($handle);
        curl_close($handle);

        throw new RuntimeException('There was an error performing the request. cURL ERROR: ' . $error_message);
    }

    /**
     * Lets just collect some extra information that can be used.
     */
    $info = curl_getinfo($handle);

    /**
     * Lets close the curl session so that we free up the server for requests by
     * others.
     */
    curl_close($handle);

    /**
     * Make suse JSON data is usable in PHP.
     */
    if ($info['content_type'] === 'application/json') {
        $response = json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * Now we format and return the response
     */
    return [
        'status_code' => $info['http_code'],
        'data' => $response,
        'info' => $info,
    ];
}

/**
 * Now that we have our handy function to perform our request for us, we can
 * perform an HTTP request.
 * We are going to be using a free API thanks to https://public-apis.io/
 */

/**
 * This just makes a simple get request.
 *
 * @return array
 * @throws JsonException
 */
function evilInsultGetJson(): array
{
    $url = 'https://evilinsult.com/generate_insult.php?lang=en&type=json';
    $data = [
        'lang' => 'en',
        'type' => 'json',
    ];
    $method = HTTP_GET;
    $headers = [
        'Accepts: application/json',
    ];

    return doRequest($url, $method, $data, $headers);
}

try {
    $response = evilInsultGetJson();
    var_dump($response);
} catch (JsonException $e) {
    var_dump("Whoops: ", $e);
}
